package com.domquentin.fitness

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import com.google.android.libraries.places.api.Places
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.drawer_toolbar.*

class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, HomeFragment.onFragmentBtnSelected {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setSupportActionBar(toolbar)

        navigationView.setNavigationItemSelectedListener(this)

        var actionBarDrawerToggle = ActionBarDrawerToggle(this, drawer, toolbar, R.string.open, R.string.close)

        drawer.addDrawerListener(actionBarDrawerToggle)
        actionBarDrawerToggle.isDrawerIndicatorEnabled = true
        actionBarDrawerToggle.syncState()

        //load default fragment
        var fragmentManager = supportFragmentManager
        var fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.fragment_container, HomeFragment())
        fragmentTransaction.commit()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        drawer.closeDrawer(GravityCompat.START)
        when (item.itemId) {
            R.id.home -> {
                loadFragment("home")
            }
            R.id.other -> {
                loadFragment("other")
            }
        }
        return false
    }

    fun loadFragment(page: String) {
        var fragmentManager = supportFragmentManager
        var fragmentTransaction = fragmentManager.beginTransaction()

        when (page) {
            "home" -> fragmentTransaction.replace(R.id.fragment_container, HomeFragment())
            "other" -> fragmentTransaction.replace(R.id.fragment_container, OtherFragment())
        }

        fragmentTransaction.commit()
    }

    override fun onButtonSelected() {
        startActivity(Intent(this, MapsActivity::class.java))

    }
}
