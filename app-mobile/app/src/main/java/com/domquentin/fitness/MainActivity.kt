package com.domquentin.fitness

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        login_btn.setOnClickListener{ y ->


            var rq = Volley.newRequestQueue(this)
            var sr =object: StringRequest(Request.Method.POST, AppInfo.web + "user_connect.php?key=devkey" ,
                Response.Listener { response ->
                    val jsonResponse = JSONObject(response)
                    var success = 0
                    try{
                        success = jsonResponse.getInt("success")
                    } catch (e: Exception) {
                        e.printStackTrace()
                        var error = jsonResponse.getJSONObject("error")
                        Toast.makeText(this, error.getString("error_comment"), Toast.LENGTH_LONG).show()
                    }
                    if (success == 1) {
                        AppInfo.email = login_email.text.toString()
                        var i = Intent(this, HomeActivity::class.java)
                        startActivity(i)
                        finish()
                    }
                },
                    Response.ErrorListener { error ->
                        Toast.makeText(this, error.message, Toast.LENGTH_LONG).show()

                })
            {
                override fun getParams(): MutableMap<String, String> {
                    var map = HashMap<String, String>()
                    map.put("email", login_email.text.toString())
                    map.put("password", login_password.text.toString())
                    return map
                }
            }
            rq.add(sr)

        }
    }
}
