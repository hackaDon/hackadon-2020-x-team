package com.domquentin.fitness

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import java.lang.ClassCastException

class HomeFragment : Fragment() {
    lateinit var listener: onFragmentBtnSelected

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_home, container, false)
        var clickme = view.findViewById<Button>(R.id.btn)
        clickme.setOnClickListener{ y ->
          listener.onButtonSelected()
        }

        var text = view.findViewById<TextView>(R.id.text)
        text.text = AppInfo.email
        return view;
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is onFragmentBtnSelected) {
            listener = context
        } else {
            throw ClassCastException(context.toString() + "must implemebt listener")
        }
    }

    interface onFragmentBtnSelected{
       fun onButtonSelected();
    }
}