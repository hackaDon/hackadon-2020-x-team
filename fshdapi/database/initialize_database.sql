//fichier d initialisation
CREATE DATABASE fshd;

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user`(
    `user_id` int(11) NOT NULL AUTO_INCREMENT,
    `user_email` varchar(255) NOT NULL,
    `user_password` varchar(255) NOT NULL,
    PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

DELIMITER $$
 
CREATE PROCEDURE check_user_exist(
    IN _user_email varchar(255),
    OUT _result BOOLEAN
)
BEGIN
 
    DECLARE __count_user int(11);

    SELECT count(*) into __count_user FROM user WHERE user_email = _user_email;
    
    IF __count_user = 0 THEN
        SET _result = 0;
    ELSE
        SET _result = 1;
    END IF;
    
END$$
 
DELIMITER ;

--DROP PROCEDURE check_user_exist;

DELIMITER $$
 
CREATE PROCEDURE get_user_password_hash(
    IN _user_email varchar(255),
    OUT _result varchar(255)
)
BEGIN
 
    DECLARE __user_password varchar(255);

    SELECT user_password into __user_password FROM user WHERE user_email = _user_email;
    
    SET _result = __user_password;
    
END$$
 
DELIMITER ;

--DROP PROCEDURE get_user_password_hash;

DELIMITER $$
 
CREATE PROCEDURE insert_user(
    IN _user_email varchar(255),
    IN _user_password varchar(255),
    OUT _result BOOLEAN
)
BEGIN
 
    DECLARE __count_user int(11);

    SELECT count(*) into __count_user FROM user WHERE user_email = _user_email;
    
    IF __count_user = 0 THEN
        INSERT INTO user(user_email, user_password) VALUES(_user_email, _user_password);
        SELECT count(*) into __count_user FROM user WHERE user_email = _user_email;
        IF __count_user = 0 THEN
            SET _result = 0;
        ELSE
            SET _result = 1;
        END IF;
    ELSE
        SET _result = 0;
    END IF;

    
END$$
 
DELIMITER ;

--DROP PROCEDURE insert_user;

DROP TABLE IF EXISTS `api_key`;
CREATE TABLE IF NOT EXISTS `api_key` (
  `api_key_id` int(11) NOT NULL AUTO_INCREMENT,
  `api_key_key` varchar(255) NOT NULL,
  `api_key_version` varchar(10) NOT NULL,
  `status_id` int(11) NOT NULL,
  `api_key_date_creation` datetime NOT NULL,
  PRIMARY KEY (`api_key_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

INSERT INTO api_key(api_key_key, api_key_version, status_id, api_key_date_creation) VALUES('devkey', 'v1', 0, NOW());

DELIMITER $$
 
CREATE PROCEDURE check_key_exist(
    IN _api_key_key VARCHAR(255),
    OUT _result BOOLEAN
)
BEGIN
 
    DECLARE __count_key int(11);

    SELECT count(*) into __count_key FROM api_key WHERE api_key_key = _api_key_key;
    
    IF __count_key = 0 THEN
        SET _result = 0;
    ELSE
        SET _result = 1;
    END IF;
    
END$$
 
DELIMITER ;

--DROP PROCEDURE check_key_exist;