DELIMITER $$
 
CREATE PROCEDURE check_key_exist(
    IN _api_key_key VARCHAR(255),
    OUT _result BOOLEAN
)
BEGIN
 
    DECLARE __count_key int(11);

    SELECT count(*) into __count_key FROM api_key WHERE api_key_key = _api_key_key;
    
    IF __count_key = 0 THEN
        SET _result = 0;
    ELSE
        SET _result = 1;
    END IF;
    
END$$
 
DELIMITER ;

--DROP PROCEDURE check_key_exist;