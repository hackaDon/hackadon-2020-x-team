DROP TABLE IF EXISTS `api_key`;
CREATE TABLE IF NOT EXISTS `api_key` (
  `api_key_id` int(11) NOT NULL AUTO_INCREMENT,
  `api_key_key` varchar(255) NOT NULL,
  `api_key_version` varchar(10) NOT NULL,
  `status_id` int(11) NOT NULL,
  `api_key_date_creation` datetime NOT NULL,
  PRIMARY KEY (`api_key_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

INSERT INTO api_key(api_key_key, api_key_version, status_id, api_key_date_creation) VALUES('devkey', 'v1', 0, NOW());