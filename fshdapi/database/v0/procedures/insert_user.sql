DELIMITER $$
 
CREATE PROCEDURE insert_user(
    IN _user_email varchar(255),
    IN _user_password varchar(255),
    OUT _result BOOLEAN
)
BEGIN
 
    DECLARE __count_user int(11);

    SELECT count(*) into __count_user FROM user WHERE user_email = _user_email;
    
    IF __count_user = 0 THEN
        INSERT INTO user(user_email, user_password) VALUES(_user_email, _user_password);
        SELECT count(*) into __count_user FROM user WHERE user_email = _user_email;
        IF __count_user = 0 THEN
            SET _result = 0;
        ELSE
            SET _result = 1;
        END IF;
    ELSE
        SET _result = 0;
    END IF;

    
END$$
 
DELIMITER ;

--DROP PROCEDURE insert_user;