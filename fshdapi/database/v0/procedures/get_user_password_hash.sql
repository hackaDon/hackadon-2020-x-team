DELIMITER $$
 
CREATE PROCEDURE get_user_password_hash(
    IN _user_email varchar(255),
    OUT _result varchar(255)
)
BEGIN
 
    DECLARE __user_password varchar(255);

    SELECT user_password into __user_password FROM user WHERE user_email = _user_email;
    
    SET _result = __user_password;
    
END$$
 
DELIMITER ;

--DROP PROCEDURE get_user_password_hash;