<?php
class User
{
    private $_main;
    private $_user_id;
    private $_user_email;
    private $_user_password;
    public $_result;

    public function __construct($main, $user_email = null)
    {
        $this->_main = $main;
        $this->_user_email = $user_email;
        if($user_email != null)
        {
            $this->_result = $this->_main->_bdd->exec_procstock($this->_main, 'check_user_exist', array($user_email), 1);
        }
    }

}