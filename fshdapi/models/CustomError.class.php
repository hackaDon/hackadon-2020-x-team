<?php
class CustomError{

    private $_error_code;
    private $_error_exception;
    private $_error_comment;

    public function __construct(String $code, String $exception, String $comment)
    {
        $this->_error_code = $code;
        $this->_error_exception = $exception;
        $this->_error_comment = $comment;
    }

    public function get_array_error() : array
    {
        $error = array();
        $error['error_code'] = $this->_error_code;
        $error['error_exception'] = $this->_error_exception;
        $error['error_comment'] = $this->_error_comment;
        return $error;
    } 

}