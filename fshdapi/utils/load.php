<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-type: application/json');

    require_once("../../config/Main.class.php");

    spl_autoload_register(function ($class_name) {
        include '../../models/' .$class_name . '.class.php';
    });

    $main = new Main();

    $api_key = $main->get_parameter("key");

    if($api_key == "")
    {
        $main->_log->write('Clé api non renseignée');
        $error = new CustomError('1', 'argument_validation', 'Missing api key');
        $json_data['error'] = $error->get_array_error(); 
        $json = json_encode($json_data, JSON_UNESCAPED_UNICODE);
        echo($json);
        exit;
    }

    $result = $main->_bdd->exec_procstock($main, 'check_key_exist', array($api_key), 1);
    if($result == "0")
    {
        $main->_log->write('Clé api inéxistante : ' . $api_key);
        $error = new CustomError('1', 'argument_validation', 'Invalid api key');
        $json_data['error'] = $error->get_array_error(); 
        $json = json_encode($json_data, JSON_UNESCAPED_UNICODE);
        echo($json);
        exit;
    }