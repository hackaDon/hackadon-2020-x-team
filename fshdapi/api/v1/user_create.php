<?php
    include("../../utils/load.php");

    $email = $main->get_parameter('email');
    $password = $main->get_parameter('password');

    if($email == "" || $password == "")
    {
        $error = new CustomError('1', 'argument_validation', 'Missing argument');
        $json_data['error'] = $error->get_array_error(); 
        $json = json_encode($json_data, JSON_UNESCAPED_UNICODE);
        echo($json);
        exit;
    }

    $options = [
        'cost' => 12,
    ];

    $pass_hash = password_hash($password, PASSWORD_BCRYPT, $options);
    $result = $main->_bdd->exec_procstock($main, 'insert_user', array(strtolower($email), $pass_hash), 1);

    if($result['_result0'] == '0')
    {
        $error = new CustomError('2', 'user_validation', 'An user already exists with this email');
        $json_data['error'] = $error->get_array_error(); 
        $json = json_encode($json_data, JSON_UNESCAPED_UNICODE);
        echo($json);
        exit;
    }
    elseif($result['_result0'] == "1")
    {
        $json_data['success'] = 1;
        $json = json_encode($json_data, JSON_UNESCAPED_UNICODE);
        echo($json);
        exit; 
    }
    else
    {
        $error = new CustomError('502', 'server_exception', 'Unknow error, please contact the support');
        $json_data['error'] = $error->get_array_error(); 
        $json = json_encode($json_data, JSON_UNESCAPED_UNICODE);
        echo($json);
        exit;
    }
