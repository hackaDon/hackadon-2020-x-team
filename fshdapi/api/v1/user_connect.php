<?php
    include("../../utils/load.php");

    $email = $main->get_post_parameter('email');

    if($email == "")
    {
        $error = new CustomError('1', 'argument_validation', 'Missing email');
        $json_data['error'] = $error->get_array_error(); 
        $json = json_encode($json_data, JSON_UNESCAPED_UNICODE);
        echo($json);
        exit;
    }

    $user = new User($main, $main->get_post_parameter('email'));
    if($user->_result['_result0'] == '0')
    {
        $error = new CustomError('2' , 'user_validation', 'This user dosen\'t exist');
        $json_data['error'] = $error->get_array_error();
        $json = json_encode($json_data, JSON_UNESCAPED_UNICODE);
        echo($json);
        exit;
    }

    $password = $main->get_post_parameter('password');
    
    if($password == "")
    {
        $error = new CustomError('1', 'argument_validation', 'Missing password');
        $json_data['error'] = $error->get_array_error(); 
        $json = json_encode($json_data, JSON_UNESCAPED_UNICODE);
        echo($json);
        exit;
    }

    $password_hash = $main->_bdd->exec_procstock($main, 'get_user_password_hash', array(strtolower($email)), true);

    if(password_verify($password, $password_hash['_result0']))
    {
        $json_data['success'] = 1;
        $json = json_encode($json_data, JSON_UNESCAPED_UNICODE);
        echo($json);
        exit; 
    }
    else
    {
        $error = new CustomError('2', 'user_validation', 'Invalid password');
        $json_data['error'] = $error->get_array_error(); 
        $json = json_encode($json_data, JSON_UNESCAPED_UNICODE);
        echo($json);
        exit; 
    }

