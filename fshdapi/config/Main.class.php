<?php
    require_once("../../config/Database.class.php");

    class Main {
        public $_bdd;

        public function __construct()
        {
            $this->_bdd = new Database();
        }

        public function get_parameter(string $param) : string
        {
            return isset($_GET[$param]) ? htmlspecialchars($_GET[$param]) : "";
        }

        public function get_post_parameter(string $param) : string
        {
            return isset($_POST[$param]) ? htmlspecialchars($_POST[$param]) : "";
        }
    }
