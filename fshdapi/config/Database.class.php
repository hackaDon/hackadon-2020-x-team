<?php
    class Database{
        private $_database_host = 'localhost';
        private $_database_name = 'fshd';
        private $_database_username = 'root';
        private $_database_password = '';
        public $_database_bdd;

        public function __construct()
        {
            $this->_database_bdd = mysqli_connect($this->_database_host ,$this->_database_username ,$this->_database_password , $this->_database_name);

            if (mysqli_connect_errno()) {
                exit;
            }
        }

        public function connect_database()
        {
            $this->_database_bdd = mysqli_connect($this->_database_host ,$this->_database_username ,$this->_database_password , $this->_database_name);

            if (mysqli_connect_errno()) {
                exit;
            }

            return $this->_database_bdd;
        }

        public function exec_procstock(Main $main, string $procedure, array $params, int $has_result=0)
        {
            $list_params = [];
            foreach($params as $key => $param)
            {
                $this->_database_bdd->query('SET @p' . $key . ' = "' . $param . '"');
                $list_params[] = "@p" .$key;    
            } 
            $query = "CALL " . $procedure . "(";
            foreach($list_params as $key => $param)
            {
                if(count($list_params) > 0)
                {
                    if(count($list_params) - 1 == $key )
                    {
                        $query .= $param;
                    }
                    else
                    {
                        $query .= $param . ", ";
                    }
                }
            }
            if($has_result > 0)
            {
                for($i = 0; $i <= $has_result-1; $i++)
                {
                    $query .= ", @_result" . $i;
                }
            }
            $query .= ")";
            $this->_database_bdd->query($query);
            if($has_result > 0)
            {
                $query = "SELECT ";
                for($i = 0; $i <= $has_result -1; $i++)
                {
                    $query .= "@_result" . $i . ' as _result' . $i . ' '; 
                }
                    $res = $this->_database_bdd->query($query);
                    $ret = $res->fetch_assoc();
                    return $ret; 
            }
        }
    }