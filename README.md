Comment utiliser le projet ?

Installez un serveur MySQL et PHP (wamp suffit)

Mettez le dossier fshdapi dans le dossier www de votre serveur PHP

Ensuite aller sur votre serveur MySQL et exécutez le fichier suivant /fshdapi/database/initialize_database.sql

Ce fichier va vous créer la base de données ainsi que les tables/procédures nécessaires pour le projet

Pour créer un utilisateur tapez cette URL : https://localhost/fsadhapi/api/v1/user_create.php?key=devkey&email=[VOTRE EMAIL]&password=[VOTRE PASSWORD]

Pour lier l'API à l'application mobile allez dans le class AppInfo.kt et changez l'url d'accès à l'API, pour qu'il fonctionne sur votre réseau wifi, ouvrer un CMD tapez ipconfig et prenez l'ipv4

Une fois tout cela fait le projet est prêt pour utilisation

